"use strict";

$(document).ready(function () {
  $(window).on('load resize scroll', function () {
    if ($(window).scrollTop() > 200) {
      /* $('.q-header').addClass('q-header_active'); */
      $('.q-to-contact').addClass('q-to-contact_active');
    } else {
      /* $('.q-header').removeClass('q-header_active'); */
      $('.q-to-contact').removeClass('q-to-contact_active');
    }

    if ($(window).scrollTop() < 1200) {
      new Swiper('.q-item__cards', {
        loop: false,
        slidesPerView: 'auto',
        breakpoints: {
          1200: {
            loop: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            spaceBetween: 20
          }
        }
      });
    }
  });
  new Swiper('.q-order-shedule .swiper-container', {
    navigation: {
      nextEl: '.q-order-shedule__next',
      prevEl: '.q-order-shedule__prev'
    },
    breakpoints: {
      768: {
        slidesPerView: 'auto',
        touchEventsTarget: 'container',
        freeMode: true,
        allowTouchMove: false
      }
    }
  });
  new Swiper('.q-item-gallery', {
    loop: true,
    autoplay: {
      delay: 3000
    },
    pagination: {
      el: '.q-item-gallery .swiper-pagination',
      clickable: true
    }
  });
  $('.phone').inputmask('+7(999)-999-99-99');
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.q-modal').toggle();
  });
  $('.q-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.q-modal').toggle();
  });
  $('.open-menu').on('click', function (e) {
    e.preventDefault();
    $('.wraper').toggleClass('wraper_active');
  });
  $('.q-header__nav').on('click', function (e) {
    e.preventDefault();
    $('.q-nav').toggle();
  });
  $('.q-nav__close').on('click', function (e) {
    e.preventDefault();
    $('.q-nav').toggle();
  });
  $('.wraper__close').on('click', function (e) {
    e.preventDefault();
    $('.wraper').toggleClass('wraper_active');
  });
  $('.q-item-collapse__header').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('q-item-collapse__header_active');
    $(this).next().slideToggle('fast');
  });
  $('.q-order-step__counter-plus').on('click', function (e) {
    e.preventDefault();
    var currentValue = $('.q-order-step__counter input').val();

    if (currentValue < 20) {
      $(this).next().val(++currentValue);
    }
  });
  $('.q-order-step__counter-minus').on('click', function (e) {
    e.preventDefault();
    var currentValue = $('.q-order-step__counter input').val();

    if (currentValue > 1) {
      $(this).prev().val(--currentValue);
    }
  });
  $('.circle-block').css('opacity', '1');
  var circleType = new CircleType(document.getElementById('text'));
  $('.menu-list .menu-item').hover(function () {
    $('.circle-block .eye .img').toggleClass('open');
  });
});
//# sourceMappingURL=main.js.map
